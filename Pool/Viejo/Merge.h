/*#ifndef MERGE_H
#define MERGE_H
#include <iostream>
#include "Constantes.h"

namespace pool
{
	//----------- De GamePlay -----------
	extern bool gameOver;
	extern int lisasEmbocadas;
	extern int ralladasEmbocadas;

	void game();

	void init();
	void input();
	void update();
	void colisiones();
	void draw();

	//----------- Logica -----------
	void moverPelota();
	bool algunaPelotaEnMovimiento();

	//----------- Colisiones -----------
	void colisionPelotaPelota();
	void colisionPelotaEsquina();
	void colisionPelotaBanda();
	void colisionPelotaHoyo();
	float distanciaEntrePuntos(Vector2 p1, Vector2 p2);
	bool chequearColisionCirCir(Vector2 p1, int p1Radio, Vector2 p2, int p2Radio);
	bool chequearColisionCirCir(float distancia, int p1Radio, int p2Radio);
	bool ChequeoPuntoCirculo(Vector2 point, Vector2 center, int radio);
	bool chequearColisionBorde(Vector2 posicionPel, float radio, Rectangle rec);

	//----------- De Palo -----------
	struct PALO
	{
		bool activo = false;
		Texture2D textura = { 0 };
		float rot = 0;
		Vector2 pos = { 0 };
		Vector2 endpos = { 0 };
	};
	extern PALO palo;

	void initPalo();
	void updatePalo();

	//----------- De Pelota -----------
	enum class TIPOPELOTA { BLANCA, LISA, RALLADA, NEGRA };

	struct PELOTAS
	{
		bool activo = false;
		TIPOPELOTA tipo = TIPOPELOTA::BLANCA;
		Color color = { 0 };

		Vector2 cir = { 0 };
		int radio = radioGral;
		Vector2 vel = { 0 };
		float masa = (float)radio;
		float velDesgaste = PerdidaPotencia;

		bool enMovimiento = false;
		bool desaparicion = false;
	};
	extern PELOTAS pelota[maxPelotas];

	void initPelotas();
	void IniciarSeteo(int setear, int seteador);
	void setearPelotaConRef(int setear, int enX, int enY);

	//----------- Bandas -----------
	enum class BANDA { ARRIBA, ABAJO, DERECHA, IZQUIERDA };

	struct ESQUINAS
	{
		Vector2 cir = { 0, 0 };
		float radio = 0;
		float masa = static_cast<float>(INT_MAX);
		Color color = BLUE;
	};
	struct BANDAS
	{
		BANDA tipo = BANDA::ARRIBA;
		Rectangle rec = { 0 };

		ESQUINAS esq[2];
	};
	extern BANDAS banda[maxBandas];

	void initBordes();
	void crearEsquinasH(int i);
	void crearEsquinasV(int i);

	//----------- Hoyos -----------	
	enum class HOYO { ARRIBAIZQ, ARRIBACEN, ARRIBADER, ABAJOIZQ, ABAJOCEN, ABAJODER };
	struct HOYOS
	{
		Vector2 pos = { 0, 0 };
		Color color = GREEN;
	};
	extern HOYOS hoyo[maxHoyos];

	void initHoyos();
	void dibujarHoyos(HOYOS cir);

	//----------- Input -----------
	extern Vector2 mouse;
	extern bool mousePrecionado;

	void hacks();

}

#endif*/