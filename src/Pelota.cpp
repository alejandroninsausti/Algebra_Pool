#include "Pelota.h"

#include <iostream>
#include <cmath>

namespace POOL {
	namespace PELOTA {

		Pelota* pelota[maxPelotas];

		static void IniciarSeteo(int setear, int seteador);
		static void SetearPelotaConRef(int setear, int enX, int enY);

		void init()
		{
			for (int i = 0; i < maxPelotas; i++)
			{
				pelota[i] = new Pelota();
				pelota[i]->setActivo(true); //activo indica si la pelota esta en la mesa
				pelota[i]->setEnMovimiento(false);
				pelota[i]->setVel({ 0, 0 });
				pelota[i]->setRadio(radioGral);

				if (i < maxPelotas / 2)
				{
					pelota[i]->setTipo(TipoPelota::LISA);
					pelota[i]->setColor(RED);
				}
				else if (i > maxPelotas / 2)
				{
					pelota[i]->setTipo(TipoPelota::RALLADA);
					pelota[i]->setColor(BLUE);
				}
				else if (i == maxPelotas / 2)
				{
					pelota[i]->setTipo(TipoPelota::NEGRA);
					pelota[i]->setColor(BLACK);
				}
			}
			pelota[0]->setTipo(TipoPelota::BLANCA);
			pelota[0]->setColor(WHITE);
			pelota[0]->setCir({ posicionInicialBlanca.x, posicionInicialBlanca.y });

			pelota[1]->setCir({ posicionInicialUno.x, posicionInicialUno.y });

			IniciarSeteo(9, 1); //se define la posicion de la pelota 9, teniendo en cuenta la posicion de la 1
			IniciarSeteo(2, 9); //posicion de la pelota 2, teniendo en cuenta la posicion de la 9
			IniciarSeteo(10, 2); //posicion de la pelota 10, teniendo en cuenta la posicion de la 2
			IniciarSeteo(3, 10); //posicion de la pelota 3, teniendo en cuenta la posicion de la 10
			SetearPelotaConRef(11, 3, 3); //se define la posicion de la pelota 11, segun la "X" y la "Y" de la 3
			SetearPelotaConRef(4, 11, 11); //posicion de la pelota 4, segun la "X" y la "Y" de la 11
			SetearPelotaConRef(12, 11, 4); //posicion de la pelota 12, segun la "X" de la 11 y la "Y" de la 4
			SetearPelotaConRef(5, 11, 12); //posicion de la pelota 5, segun la "X" de la 11 y la "Y" de la 12
			SetearPelotaConRef(15, 10, 10); //posicion de la pelota 15, segun la "X" y la "Y" de la 10
			SetearPelotaConRef(7, 15, 15); //posicion de la pelota 7, segun la "X" y la "Y" de la 15
			SetearPelotaConRef(13, 7, 7); //posicion de la pelota 13, segun la "X" y la "Y" de la 7
			SetearPelotaConRef(8, 2, 2); //posicion de la pelota 8, segun la "X" y la "Y" de la 2
			SetearPelotaConRef(6, 8, 8); //posicion de la pelota 6, segun la "X" y la "Y" de la 8
			SetearPelotaConRef(14, 9, 9); //posicion de la pelota 14, segun la "X" y la "Y" de la 9
		}

		const float minVel = 0.15f;
		void update() {

			for (int i = 0; i < maxPelotas; i++)
			{
				if (pelota[i]->getActivo() && pelota[i]->getEnMovimiento())
				{
					pelota[i]->mover();

					if (fabsf(pelota[i]->getVel().x) < minVel && fabsf(pelota[i]->getVel().y) < minVel)
					{   // ----- Que pare -----
						std::cout << "Pelota " << i << " ha parado." << std::endl;
						pelota[i]->setEnMovimiento(false);
					}
				}
			}
		}

		void draw() {

			for (int i = 0; i < maxPelotas; i++)
			{
				if (i > 0)
				{
					pelota[i]->dibujar(i);
				}
				else
				{
					pelota[i]->dibujar();
				}
			}
		}

		void deInit()
		{
			for (short i = 0; i < maxPelotas; i++)
			{
				if (pelota[i] != NULL)
				{
					delete pelota[i];
					pelota[i] = NULL;
				}
			}			
		}

		Pelota::Pelota()
		{
			this->activo = false;
			this->tipo = TipoPelota::BLANCA; //todas se inician a blanca, despues se les asigna el tipo correcto
			this->color = Color();

			this->cir = Vector2();
			this->radio = radioGral;
			this->vel = Vector2();
			this->masa = (float)radio;
			this->velDesgaste = PerdidaPotencia;

			this->enMovimiento = false;
			this->desaparicion = false;
		}

		Pelota::~Pelota()
		{
		}

		void Pelota::mover()
		{
			cir.x += vel.x; //se le suma la velocidad en x a la posicion en x
			cir.y += vel.y; //lo mismo en y

			vel.x *= velDesgaste; //se multiplica por un valor apenas inferior a 1.0f, para que pierda velocidad
			vel.y *= velDesgaste; //lo mismo
		}

		void Pelota::dibujar()
		{
			DrawCircleV(cir, radio, color);
		}

		void Pelota::dibujar(int numero)
		{
			dibujar();
			DrawText
			(
				TextFormat("%i", numero), //texto
				(cir.x - (MeasureText(TextFormat("%i", numero), radio) / 2)), //x - (largo de texto)/2
				cir.y - radio / 2, //y - (tama�o de fuente)/2
				radio, //tama�o de fuente
				WHITE //color
			);
		}

		bool Pelota::getActivo()
		{
			return this->activo;
		}

		void Pelota::setActivo(bool activo)
		{
			this->activo = activo;
		}

		TipoPelota Pelota::getTipo()
		{
			return this->tipo;
		}

		void Pelota::setTipo(TipoPelota tipo)
		{
			this->tipo = tipo;
		}

		Color Pelota::getColor()
		{
			return this->color;
		}

		void Pelota::setColor(Color color)
		{
			this->color = color;
		}

		Vector2 Pelota::getCir()
		{
			return this->cir;
		}

		void Pelota::setCir(Vector2 cir)
		{
			this->cir = cir;
		}

		int Pelota::getRadio()
		{
			return this->radio;
		}

		void Pelota::setRadio(int radio)
		{
			this->radio = radio;
		}

		Vector2 Pelota::getVel()
		{
			return this->vel;
		}

		void Pelota::setVel(Vector2 vel)
		{
			this->vel = vel;
		}

		float Pelota::getMasa()
		{
			return this->masa;
		}

		void Pelota::setMasa(float masa)
		{
			this->masa = masa;
		}

		float Pelota::getVelDesgaste()
		{
			return this->velDesgaste;
		}

		void Pelota::setVelDesgaste(float velDesgaste)
		{
			this->velDesgaste = velDesgaste;
		}

		bool Pelota::getEnMovimiento()
		{
			return this->enMovimiento;
		}

		void Pelota::setEnMovimiento(bool enMovimiento)
		{
			this->enMovimiento = enMovimiento;
		}

		bool Pelota::getDesaparicion()
		{
			return this->desaparicion;
		}

		void Pelota::setDesaparicion(bool desaparicion)
		{
			this->desaparicion = desaparicion;
		}

		const float raizDe3 = 1.732050f;
		const float raizDe3PorRadio = raizDe3 * radioGral;
		void IniciarSeteo(int setear, int seteador)
		{
			//https://docs.google.com/presentation/d/1geWfuWbgoPmO6Y_jzNwV_nsNYIuLzgNQn8ybOiah89Q/edit?usp=sharing
			//referencia
			pelota[setear]->setCir
			(
				{
				pelota[seteador]->getCir().x + raizDe3PorRadio + separacionBolas, //x
				pelota[seteador]->getCir().y - radioGral - 1 //y
				}
			);
		}

		void SetearPelotaConRef(int setear, int enX, int enY)
		{
			pelota[setear]->setCir
			(
				{ 
					pelota[enX]->getCir().x, //misma x que la anterior (la x por referencia)
					pelota[enY]->getCir().y + radioGral * 2 + separacionBolas //y de la anterior + diametro + extra
				}
			);
		}
	}
}