#pragma once
#include "raylib.h"

namespace POOL {
	namespace PALO {

		void init();
		void update();
		void draw();
		void deInit();

		class Palo
		{
		public:
			Palo();
			Palo(Texture2D textura, Vector2 pos);
			~Palo();

			void show();

			bool getActivo();
			void setActivo(bool activo);
			Texture2D getTextura();
			void setTextura(Texture2D textura);
			float getRot();
			void setRot(float rot);
			Vector2 getPos();
			void setPos(Vector2 pos);
			Vector2 getEndPos();
			void setEndPos(Vector2 endPos);

		private:
			bool activo;
			Texture2D textura;
			float rot;
			Vector2 pos;
			Vector2 endpos;
		};
		extern Palo* palo;
	}
}