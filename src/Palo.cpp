#include "Palo.h"

#include <cmath>

#include "Pelota.h"

namespace POOL {
    namespace PALO {

        Palo* palo;
        Texture2D paloTexture;

        void init()
        {
            paloTexture = LoadTexture("res/PaloPool.png");
            palo = new Palo(paloTexture, { 0.0f,0.0f });
        }

        void update()
        {
            float ladoOpuesto = GetMousePosition().y - PELOTA::pelota[0]->getCir().y; //y
            float ladoAdyacente = GetMousePosition().x - PELOTA::pelota[0]->getCir().x; //x

            palo->setRot
            (
                atan2(ladoOpuesto, ladoAdyacente)
                //arcotangengte de 2 parametros 
                //(devuelve el angulo entre eje x positivo [angulo 0] y la linea trazada entre el eje de coordenadas y el punto indicado)
                * 
                180 / PI //se usa para pasar de radianes a grados
            );
        }

        void draw()
        {
            palo->show();
        }

        void deInit()
        {
            if (palo != NULL)
            {
                delete palo;
                palo = NULL;
            }
            UnloadTexture(paloTexture);
        }

        Palo::Palo()
        {
            this->activo = false; //activo indica si esta o no en pantalla
            this->textura = Texture2D();
            this->rot = 0.0f;
            this->pos = Vector2();
            this->endpos = Vector2();
        }

        Palo::Palo(Texture2D textura, Vector2 pos)
        {
            this->activo = false;
            this->textura = textura;
            this->rot = 0.0f;
            this->pos = pos;
            this->endpos = pos;
        }

        Palo::~Palo()
        {

        }

        const float auxMult = 100 / 90;
        void Palo::show()
		{
			Vector2 endpos = //posicion de la punta del palo (la que le pega a la pelota)
			{
				PELOTA::pelota[0]->getCir().x
				+
				(GetMousePosition().x - PELOTA::pelota[0]->getCir().x) / 2, //devuelva la mitad de la distancia entre mouse y pelota
                //x
                //y
				PELOTA::pelota[0]->getCir().y
				+
				(GetMousePosition().y - PELOTA::pelota[0]->getCir().y) / 2 //devuelva la mitad de la distancia entre mouse y pelota
			};

            if (activo)
            {
                float porc = 0;
#if DEBUG
                DrawLineEx(PELOTA::pelota[0]->getCir(), GetMousePosition(), 1, WHITE); //dibuja linea entre pelota y borde del palo
#endif
                //correccion de posicion del palo, teniendo en cuenta rotacion               
                if (rot > 0 && rot <= 90) //derecha|abajo
                {
                    porc = rot * auxMult; //se utiliza la rotacion normal
                    endpos.x += (textura.height / 2) * porc / 100; //se le SUMA el porcentaje de la mitad del alto 
                    porc = 100 - rot * auxMult; //a 100 se le resta la rotacion, la cu�l var�a entre 0 y 90, logrando que el palo siempre este en mitad del mouse
                    endpos.y -= (textura.height / 2) * porc / 100; //se le RESTA el porcentaje de la mitad del alto
                }
                else if (rot >= -90 && rot <= 0) //derecha|arriba
                {
                    porc = abs(rot) * auxMult; //se consigue el valor absoluto de la rotacion, ya que el angulo es el mismo, pero invertido
                    endpos.x -= (textura.height / 2) * porc / 100; //se le RESTA el porcentaje de la mitad del alto
                    porc = 100 - abs(rot) * auxMult; //lo mismo que en derecha|abajo, pero con valor absoluto
                    endpos.y -= (textura.height / 2) * porc / 100; //se le RESTA el porcentaje de la mitad del alto
                }
                else if (rot > -180 && rot < -90) //izquierda|arriba 
                {
                    porc = 100 - (abs(rot) - 90) * auxMult; //los valores son iguales que derecha|arriba, pero menos 90, as� que se usa el mismo procedimiento, menos 90
                    endpos.x -= (textura.height / 2) * porc / 100; //se le RESTA el porcentaje de la mitad del alto
                    porc = (abs(rot) - 90) * auxMult;
                    endpos.y += (textura.height / 2) * porc / 100; //se le SUMA el porcentaje de la mitad del alto
                }
                else if (rot >= 90 && rot <= 180) //izquierda|abajo
                {
                    porc = 100 - (rot - 90) * auxMult; //los valores son iguales que derecha|abajo, pero m�s 90, as� que se usa el mismo procedimiento, menos 90
                    endpos.x += (textura.height / 2) * porc / 100; //se le SUMA el porcentaje de la mitad del alto
                    porc = (rot - 90) * auxMult;
                    endpos.y += (textura.height / 2) * porc / 100; //se le SUMA el porcentaje de la mitad del alto
                }

                DrawTextureEx(textura, endpos, rot, 1, WHITE);
            }
        }

        bool Palo::getActivo()
        {
            return this->activo;
        }

        void Palo::setActivo(bool activo)
        {
            this->activo = activo;
        }

        Texture2D Palo::getTextura()
        {
            return this->textura;
        }

        void Palo::setTextura(Texture2D textura)
        {
            this->textura = textura;
        }

        float Palo::getRot()
        {
            return this->rot;
        }

        void Palo::setRot(float rot)
        {
            this->rot = rot;
        }

        Vector2 Palo::getPos()
        {
            return this->pos;
        }

        void Palo::setPos(Vector2 pos)
        {
            this->pos = pos;
        }

        Vector2 Palo::getEndPos()
        {
            return this->endpos;
        }

        void Palo::setEndPos(Vector2 endPos)
        {
            this->endpos = endpos;
        }
    }
}