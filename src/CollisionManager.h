#pragma once
#include "raylib.h"

namespace POOL {
namespace COLLISION_MANAGER {

	void collisionManager();

	bool chequeoPuntoCirculo(Vector2 point, Vector2 center, int radio);
}
}